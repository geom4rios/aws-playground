package com.geom4rios.lambda.common.model;

import lombok.*;
import org.json.simple.JSONObject;

import java.util.Objects;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImportProductSplitFileDetails {

    private String name;
    private int sizeInKB;
    private boolean isLastFile;

    // create by build json plugin
    public JSONObject toJson() {
        JSONObject jo = new JSONObject();
        jo.put("name", name);
        jo.put("sizeInKB", sizeInKB);
        jo.put("isLastFile", isLastFile);
        return jo;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ImportProductSplitFileDetails that = (ImportProductSplitFileDetails) o;
        return sizeInKB == that.sizeInKB && isLastFile == that.isLastFile && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, sizeInKB, isLastFile);
    }
}

package com.geom4rios.lambda.common.properties;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Getter
public class ImportProductProperties extends PropertiesLoader {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private static volatile ImportProductProperties importProductProperties;

    private ImportProductProperties() {
        init();
    }

    public static ImportProductProperties getInstance() {
        if (importProductProperties == null) {
            synchronized (ImportProductProperties.class) {
                if (importProductProperties == null) {
                    importProductProperties = new ImportProductProperties();
                }
            }
        }
        return importProductProperties;
    }

    private void init() {
        Map<String, String> importProductPropertiesMap = new HashMap<>();
        loadProperties(importProductPropertiesMap, "import-product", "IMPORT PRODUCT PROPERTIES");
        this.s3Bucket = importProductPropertiesMap.get("s3Bucket");
        this.fileName = importProductPropertiesMap.get("fileName");
        String hasHeaderStr = importProductPropertiesMap.get("hasHeader");
        this.hasHeader = Boolean.parseBoolean(hasHeaderStr);
        this.fields = importProductPropertiesMap.get("fields");
    }

    private String s3Bucket;
    private String fileName;
    private boolean hasHeader;
    private String fields;

}

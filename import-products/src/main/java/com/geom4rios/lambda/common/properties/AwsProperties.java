package com.geom4rios.lambda.common.properties;

import lombok.Getter;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

@Getter
public class AwsProperties extends PropertiesLoader {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private static volatile AwsProperties awsProperties;

    private AwsProperties() {
        init();
    }

    public static AwsProperties getInstance() {
        if (awsProperties == null) {
            synchronized (AwsProperties.class) {
                if (awsProperties == null) {
                    awsProperties = new AwsProperties();
                }
            }
        }
        return awsProperties;
    }

    private void init() {
        Map<String, String> awsPropertiesMap = new HashMap<>();
        loadProperties(awsPropertiesMap, "aws", "AWS PROPERTIES");
        this.awsAccessKey = awsPropertiesMap.get("aws.access.key");
        this.awsSecretKey = awsPropertiesMap.get("aws.secret.key");
        this.awsRegion = awsPropertiesMap.get("aws.region");
    }

    private String awsSecretKey;
    private String awsAccessKey;
    private String awsRegion;

}

package com.geom4rios.lambda.common.utils;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.logging.Logger;

public class CustomObjectMapper extends ObjectMapper {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private static volatile CustomObjectMapper customObjectMapper;

    private CustomObjectMapper() {}

    public static CustomObjectMapper getInstance() {
        if (customObjectMapper == null) {
            synchronized (CustomObjectMapper.class) {
                if (customObjectMapper == null) {
                    customObjectMapper = new CustomObjectMapper();
                }
            }
        }
        return customObjectMapper;
    }

}

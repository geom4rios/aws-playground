package com.geom4rios.lambda.common;

import software.amazon.awssdk.auth.credentials.EnvironmentVariableCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.s3.S3Client;
import software.amazon.awssdk.services.s3.model.CopyObjectRequest;
import software.amazon.awssdk.services.s3.model.CopyObjectResponse;
import software.amazon.awssdk.services.s3.model.S3Exception;
import software.amazon.awssdk.transfer.s3.FileDownload;
import software.amazon.awssdk.transfer.s3.FileUpload;
import software.amazon.awssdk.transfer.s3.S3TransferManager;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.nio.file.Paths;

public class S3ObjectHandler {

    // copy an object from one Amazon Simple Storage Service (Amazon S3) bucket to another

    /**
     *
     * Copy an object from one Amazon Simple Storage Service (Amazon S3) bucket to another
     *
     * @param objectKey the name of the object (for example, book.pdf).
     * @param fromBucket the S3 bucket name that contains the object (for example, bucket1).
     * @param toBucket the S3 bucket to copy the object to (for example, bucket2).
     */
    public static void copyBucketObjectFromOneS3toAnother(String objectKey, String fromBucket, String toBucket) {
        System.out.format("Copying object %s from bucket %s to %s\n",
                objectKey, fromBucket, toBucket);

        Region region = Region.US_EAST_1;
        S3Client s3 = S3Client.builder()
                .region(region)
                .build();

        copyBucketObject (s3, fromBucket, objectKey, toBucket);
        s3.close();
    }


    private static String copyBucketObject (S3Client s3, String fromBucket, String objectKey, String toBucket) {

        String encodedUrl = null;
        try {
            encodedUrl = URLEncoder.encode(fromBucket + "/" + objectKey, StandardCharsets.UTF_8.toString());
        } catch (UnsupportedEncodingException e) {
            System.out.println("URL could not be encoded: " + e.getMessage());
        }
        CopyObjectRequest copyReq = CopyObjectRequest.builder()
                .copySource(encodedUrl)
                .destinationBucket(toBucket)
                .destinationKey(objectKey)
                .build();

        try {
            CopyObjectResponse copyRes = s3.copyObject(copyReq);
            return copyRes.copyObjectResult().toString();
        } catch (S3Exception e) {
            System.err.println(e.awsErrorDetails().errorMessage());
            System.exit(1);
        }
        return "";
    }

    /**
     *
     * upload an object to an Amazon Simple Storage Service (Amazon S3) bucket.
     *
     * @param bucketName the Amazon S3 bucket to upload an object into
     * @param objectKey the object to upload (for example, book.pdf).
     * @param objectPath the path where the file is located (for example, C:/AWS/book2.pdf).
     */
    public static void uploadObjectToS3(String bucketName, String objectKey, String objectPath) {
        long MB = 1024;
        System.out.println("Putting an object into bucket "+bucketName +" using the S3TransferManager");
        Region region = Region.US_EAST_1;
        S3TransferManager transferManager =  S3TransferManager.builder()
                .s3ClientConfiguration(cfg ->cfg.region(region)
                        .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                        .targetThroughputInGbps(20.0)
                        .minimumPartSizeInBytes(10 * MB))
                .build();

        uploadObjectTM(transferManager, bucketName,  objectKey, objectPath);
        System.out.println("Object was successfully uploaded using the Transfer Manager.");
        transferManager.close();
    }

    private static void uploadObjectTM( S3TransferManager transferManager,  String bucketName,  String objectKey, String objectPath) {

        FileUpload upload =
                transferManager.uploadFile(u -> u.source(Paths.get(objectPath))
                        .putObjectRequest(p -> p.bucket(bucketName).key(objectKey)));
        upload.completionFuture().join();
    }


    /**
     *
     * download an object from an Amazon Simple Storage Service (Amazon S3) bucket.
     *
     * @param bucketName the Amazon S3 bucket to upload an object into.
     * @param objectKey the object to download (for example, book.pdf).
     * @param objectPath  the path where the file is written (for example, C:/AWS/book2.pdf).
     */
    public static void downloadObjectFromS3(String bucketName, String objectKey, String objectPath) {

        long MB = 1024;
        Region region = Region.US_EAST_1;
        S3TransferManager transferManager =  S3TransferManager.builder()
                .s3ClientConfiguration(cfg ->cfg.region(region)
                        .credentialsProvider(EnvironmentVariableCredentialsProvider.create())
                        .targetThroughputInGbps(20.0)
                        .minimumPartSizeInBytes(10 * MB))
                .build();

        downloadObjectTM(transferManager, bucketName,  objectKey, objectPath);
        System.out.println("Object was successfully downloaded using the Transfer Manager.");
        transferManager.close();
    }

    private static void downloadObjectTM(S3TransferManager transferManager, String  bucketName, String objectKey, String objectPath ) {
        FileDownload download =
                transferManager.downloadFile(d -> d.getObjectRequest(g -> g.bucket(bucketName).key(objectKey))
                        .destination(Paths.get(objectPath)));
        download.completionFuture().join();

    }


}

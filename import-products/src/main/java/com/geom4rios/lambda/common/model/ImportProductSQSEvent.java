package com.geom4rios.lambda.common.model;

import com.amazonaws.util.StringUtils;
import lombok.*;
import org.json.simple.JSONObject;

import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class ImportProductSQSEvent {

    private String s3Bucket;
    private String siteDomain;
    private String publicApiKey;
    private String fileName;
    private boolean hasHeader;
    private String separator;
    private String imageSiteDir;
    private String userEmail;
    private String privateAPI;
    private String privateAPIKey;
    private JSONObject fields;
    private String pathOfFilesInS3;
    private int numberOfSplitFiles;
    private Set<ImportProductSplitFileDetails> splitFiles;
    private int filesProcessedSoFar;
    private ImportProductSplitFileDetails nextSplitFileToProcess;

    // create by build json plugin
    public JSONObject toJson() {
        JSONObject jo = new JSONObject();
        jo.put("s3Bucket", s3Bucket);
        jo.put("siteDomain", siteDomain);
        jo.put("pathOfFilesInS3", pathOfFilesInS3);
        jo.put("numberOfSplitFiles", numberOfSplitFiles);
        jo.put("splitFiles", splitFiles);
        jo.put("filesProcessedSoFar", filesProcessedSoFar);
        jo.put("nextSplitFileToProcess", nextSplitFileToProcess);
        return jo;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    public boolean isValid() {
        return !StringUtils.isNullOrEmpty(this.siteDomain) &&
                !StringUtils.isNullOrEmpty(this.publicApiKey) &&
                !StringUtils.isNullOrEmpty(this.s3Bucket) &&
                !StringUtils.isNullOrEmpty(this.fileName) &&
                !StringUtils.isNullOrEmpty(this.separator) &&
                !StringUtils.isNullOrEmpty(this.userEmail) &&
                !StringUtils.isNullOrEmpty(this.privateAPI) &&
                this.fields != null &&
                !this.fields.isEmpty();
    }
}

package com.geom4rios.lambda.common.exception;

public class ReadKeyFromPropertiesException extends RuntimeException {
    public ReadKeyFromPropertiesException(String message) {
        super(message);
    }
}

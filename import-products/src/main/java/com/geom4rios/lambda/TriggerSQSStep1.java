package com.geom4rios.lambda;

import com.geom4rios.lambda.common.S3ObjectHandler;
import com.geom4rios.lambda.common.SQSService;
import com.geom4rios.lambda.common.model.ImportProductSQSEvent;
import com.geom4rios.lambda.common.properties.ImportProductProperties;
import com.geom4rios.lambda.common.utils.CustomObjectMapper;
import org.json.simple.JSONObject;

import java.io.IOException;
import java.util.HashMap;

public class TriggerSQSStep1 {

    public static void main(String[] args) throws IOException {

        TriggerSQSStep1 triggerSQSStep1 = new TriggerSQSStep1();
        ImportProductProperties importProductProperties = ImportProductProperties.getInstance();

        //String fullFileName = "C:\\temp\\import_products.csv";
        // upload file to S3
        String fileName = "import_products.csv";
        String filePath = "C:\\temp";
        triggerSQSStep1.uploadCsvFileToS3(importProductProperties.getS3Bucket(), fileName, filePath);

        // create ImportProducts SQS Event
        ImportProductSQSEvent importProductSQSEvent = triggerSQSStep1.createImportProductSQSEvent();

        // send SQS message
        String sqsQueueName = "marios-import-products";
        triggerSQSStep1.sendSqsMessageToTriggerLambdaOne(sqsQueueName, importProductSQSEvent.toString());
    }

    public ImportProductSQSEvent createImportProductSQSEvent() throws IOException {
        ImportProductProperties importProductProperties = ImportProductProperties.getInstance();
        JSONObject fields = getFields(importProductProperties.getFields());
        return ImportProductSQSEvent.builder()
                .fields(fields)
                .fileName(importProductProperties.getFileName())
                .hasHeader(importProductProperties.isHasHeader())
                .s3Bucket(importProductProperties.getS3Bucket())
                .build();
    }

    public void uploadCsvFileToS3(String S3bucketName, String fileName, String filePath) {
        S3ObjectHandler.uploadObjectToS3(S3bucketName, fileName, filePath);
    }

    public void sendSqsMessageToTriggerLambdaOne(String queueName, String message) {
        SQSService.sendMessageToAnAmazonSQSQueue(queueName, message);
    }

    private JSONObject getFields(String fields) throws IOException {
        CustomObjectMapper customObjectMapper = CustomObjectMapper.getInstance();
        HashMap fieldsMap = customObjectMapper.readValue(fields, HashMap.class);
        return new JSONObject(fieldsMap);
    }

}

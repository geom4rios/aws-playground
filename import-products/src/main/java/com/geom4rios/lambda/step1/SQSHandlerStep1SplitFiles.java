package com.geom4rios.lambda.step1;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.SQSEvent;

public class SQSHandlerStep1SplitFiles implements RequestHandler<SQSEvent, Void> {

    @Override
    public Void handleRequest(SQSEvent sqsEvent, Context context) {
        // todo receive SQS event
        // todo read file from S3
        // todo split files into multiple parts & upload each part to S3
        // todo create SQS event and notify SQSHandlerStep2 to read first partial/split file
        return null;
    }

}

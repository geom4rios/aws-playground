package com.simvoly.utils;

import com.simvoly.exception.ReadKeyFromConfigMapException;
import org.apache.commons.lang3.StringUtils;

import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Logger;

public class ConfigurationLoader {

    Logger logger = Logger.getLogger(this.getClass().getName());

    private static volatile ConfigurationLoader configurationLoader;

    private ConfigurationLoader() {}

    public static ConfigurationLoader getInstance() {
        if (configurationLoader == null) {
            synchronized (SharedAwsCredentialsProvider.class) {
                if (configurationLoader == null) {
                    return new ConfigurationLoader();
                }
            }
        }
        return configurationLoader;
    }

    public void loadConfiguration(Map<String, String> map, String fileName, String message, boolean consoleOutput) {
        ResourceBundle rb = ResourceBundle.getBundle(fileName);
        Set<String> keys = rb.keySet();
        keys.forEach(key -> {
            String valueFromEnvFile = rb.getString(key);
            String envVarName = StringUtils.substringBetween(valueFromEnvFile, "${", "}");
            String variableValue = System.getenv(envVarName);
            map.put(key, variableValue);
        });
        if (consoleOutput) {
            logger.info("######### " + message + " #########");
            logger.info(map.toString());
            logger.info("###################################");
        }
    }

    public String readKeyFromConfigMap(Map<String, String> map, String keyName, String appName) {
        if (map.containsKey(keyName)) {
            String apiKeyFromConfiguration = map.get(keyName);
            if (apiKeyFromConfiguration.isEmpty() || apiKeyFromConfiguration.isBlank()) {
                String errMsg = String.format("Key %s for %s is empty or blank", keyName, appName);
                throw new ReadKeyFromConfigMapException(errMsg);
            } else {
                return apiKeyFromConfiguration;
            }
        } else {
            String errMsg = String.format("No key %s provided for %s", keyName, appName);
            throw new ReadKeyFromConfigMapException(errMsg);
        }
    }
}

package com.simvoly.utils;

import java.util.HashMap;
import java.util.Map;

public class SharedAwsCredentialsProvider {

    private static volatile SharedAwsCredentialsProvider sharedAwsCredentialsProvider;
    private Map<String, String> awsCredentials = new HashMap<>();
    private String awsRegion;
    private String accessKey;
    private String secretKey;

    private SharedAwsCredentialsProvider() {
        init();
    }

    private void init() {
        ConfigurationLoader configurationLoader = ConfigurationLoader.getInstance();
        configurationLoader.loadConfiguration(awsCredentials, "aws", "", false);
        instantiateAWSCredentials();
        instantiateAWSRegion();
    }

    private void instantiateAWSCredentials() {
        // todo validate key exist, not empty etc
        this.accessKey = awsCredentials.get("aws.access.key");;
        this.secretKey = awsCredentials.get("aws.secret.key");;
    }

    private void instantiateAWSRegion() {
        // todo validate key exist, not empty etc
        String awsRegion = awsCredentials.get("aws.region");
        this.awsRegion = awsRegion;
    }

    public static SharedAwsCredentialsProvider getInstance() {
        if (sharedAwsCredentialsProvider == null) {
            synchronized (SharedAwsCredentialsProvider.class) {
                if (sharedAwsCredentialsProvider == null) {
                    return new SharedAwsCredentialsProvider();
                }
            }
        }
        return sharedAwsCredentialsProvider;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public String getAccessKey() {
        return accessKey;
    }

    public String getSecretKey() {
        return secretKey;
    }
}

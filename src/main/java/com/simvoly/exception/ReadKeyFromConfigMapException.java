package com.simvoly.exception;

public class ReadKeyFromConfigMapException extends RuntimeException {
    public ReadKeyFromConfigMapException(String message) {
        super(message);
    }
}

package com.simvoly.amazonSQS;

import com.simvoly.utils.SharedAwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.sqs.SqsClient;

public class SqsClientProvider {

    private static volatile SqsClientProvider sqsClientProvider;
    private final StaticCredentialsProvider staticCredentialsProvider;

    private SqsClientProvider() {
        String accessKey = SharedAwsCredentialsProvider.getInstance().getAccessKey();
        String secretKey = SharedAwsCredentialsProvider.getInstance().getSecretKey();
        this.staticCredentialsProvider = StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey));
    }

    public static SqsClientProvider getInstance() {
        if (sqsClientProvider == null) {
            synchronized (SqsClientProvider.class) {
                if (sqsClientProvider == null) {
                    return new SqsClientProvider();
                }
            }
        }
        return sqsClientProvider;
    }

    public SqsClient getNewSqsClient() {
        String awsRegion = SharedAwsCredentialsProvider.getInstance().getAwsRegion();
        return SqsClient.builder()
                .credentialsProvider(this.staticCredentialsProvider)
                .region(Region.of(awsRegion))
                .build();
    }

}

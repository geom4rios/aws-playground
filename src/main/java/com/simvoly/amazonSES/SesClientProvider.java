package com.simvoly.amazonSES;

import com.simvoly.amazonSQS.SqsClientProvider;
import com.simvoly.utils.SharedAwsCredentialsProvider;
import software.amazon.awssdk.auth.credentials.AwsBasicCredentials;
import software.amazon.awssdk.auth.credentials.StaticCredentialsProvider;
import software.amazon.awssdk.regions.Region;
import software.amazon.awssdk.services.ses.SesClient;

public class SesClientProvider {

    private static volatile SesClientProvider sesClientProvider;
    private final StaticCredentialsProvider staticCredentialsProvider;

    private SesClientProvider() {
        String accessKey = SharedAwsCredentialsProvider.getInstance().getAccessKey();
        String secretKey = SharedAwsCredentialsProvider.getInstance().getSecretKey();
        this.staticCredentialsProvider = StaticCredentialsProvider.create(AwsBasicCredentials.create(accessKey, secretKey));
    }

    public static SesClientProvider getInstance() {
        if (sesClientProvider == null) {
            synchronized (SesClientProvider.class) {
                if (sesClientProvider == null) {
                    return new SesClientProvider();
                }
            }
        }
        return sesClientProvider;
    }

    public SesClient getNewSesClient() {
        String awsRegion = SharedAwsCredentialsProvider.getInstance().getAwsRegion();
        return SesClient.builder()
                .region(Region.of(awsRegion))
                .credentialsProvider(this.staticCredentialsProvider)
                .build();
    }

}
